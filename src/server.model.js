const {Schema, model} = require('mongoose')

const Server = new Schema({
    servers: [{
        type: String,
        required: true
    }],
    proxyPass: {
        type: String,
        required: true
    },
    key: {
        type: String,
        required: true,
        index: true,
        unique: true
    }
})

module.exports = model('Server', Server)