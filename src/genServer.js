module.exports = (names, proxypass) => {
    let namesCtx = ""
    names.forEach((element, i) => {
        namesCtx += i == names.length - 1 ? element : element+' '
    });
    return `
server {
    listen 80;
    listen [::]:80;

    server_name ${namesCtx};

    location / {
        proxy_pass          http://${proxypass};
        proxy_set_header    Host \\$host;
        proxy_set_header    X-Real-IP \\$remote_addr;
        proxy_set_header    X-Forwarded-For \\$proxy_add_x_forwarded_for;
        proxy_set_header    X-Forwarded-Proto \\$scheme;
        proxy_redirect      off;
    }
}`
}