const sh = require('shelljs')
const genServer = require('./genServer')

require('./database')

const express = require('express')
const path = require('path')
const cors = require('cors')
const app = express()

const Server = require('./server.model')

const NGINX_AVAILABLE_PATH = process.env.NODE_ENV == 'production' ? path.join('/','etc','nginx','sites-available') : path.join(process.cwd() , 'servers')
const NGINX_ENABLED_PATH = process.env.NODE_ENV == 'production' ? path.join('/','etc','nginx','sites-enabled') : path.join(process.cwd() , 'serversprod')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: false}))

app.route('/server')
.post(async (req, res, next) => {
    try {
        const {servers, proxyPass, key} = req.body
        if (!key || key == '' || !servers || typeof servers != 'object' || servers.length <= 0 || !proxyPass)
            return res.json({info: 'invalid data structure'})
        for (const element of servers) {
            const validation = await Server.findOne({servers: element})
            if (validation && validation._id) {return res.json({info: 'this server is already saved', data: element})}
            if (typeof element != 'string' || !element) {return res.json({info: 'invalid data type'})}
        }
        let othervalidation = await Server.findOne({key})
        if (othervalidation && othervalidation._id)
            return res.status(400).json({success: false, info: 'This key is already saved'})
        let data = new Server(req.body)
        var error = data.validateSync()
        if (error) {
            let status = []
            for (const key in error.errors) {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi, "")
                };
                status.push(res);
            }
            return res.status(400).json({ success: false, info: 'Invalid data structure', data: status })
        } else {
            const status = await data.save();
            if (!('_id' in status)) {
                return res.status(400).json({ success: false, info: 'Fatal Error, unable to store server, try later' })
            } else {
                let cd = sh.cd(NGINX_AVAILABLE_PATH)
                // if (cd.stderr != '' && stderr != null) {
                //     return res.status(400).json({success: false, info: 'Error changing directory'})
                // }
                let shellString = genServer(servers, proxyPass) //replace this to "sh.exec(`sudo sh -c 'echo ${shellstring} >> filepath'`)" //this work for all functions that need sudo instead default functions
                let createServer = sh.exec(`sudo sh -c 'echo "${shellString}" >> ${status.key.toString()}'`, {silent: true})
                if (createServer.stderr != '' && stderr != null) {
                    return res.status(400).json({success: false, info: 'Error creating server file'})
                }
                // let ln = sh.ln('-s', path.join(NGINX_AVAILABLE_PATH, status._id.toString()), path.join(NGINX_ENABLED_PATH, status._id.toString()))
                let symlink = sh.exec(`sudo sh -c 'ln -s ${path.join(NGINX_AVAILABLE_PATH, status.key.toString())} ${path.join(NGINX_ENABLED_PATH, status.key.toString())}'`, {silent: true})
                if (symlink.stderr != '' && stderr != null) {
                    return res.status(400).json({success: false, info: 'Error creating symlink'})
                }
                let domains = ''
                for (const domi of servers) {
                    domains += ` -d ${domi}`
                }
                let certs = sh.exec(`sudo certbot --nginx -n --redirect${domains}`, {silent: true})
                // if (certs.stderr != '' && stderr != null) {
                //     return res.status(400).json({success: false, info: 'Error generating SSL certificates'})
                // }
                let reload = sh.exec(`sudo systemctl reload nginx`, {silent: true})
                if (reload.stderr != '' && stderr != null) {
                    return res.status(400).json({success: false, info: 'Error restarting nginx service'})
                }
                return res.status(200).json({ success: true, info: 'Server save successfully', data: status })
            }
        }
    } catch (error) {
        return res.status(400).json({success: false, info: 'Internal error', data: error})
    }
})
.get(async (req, res, next) => {
    try {
        let data = await Server.find()
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        return res.status(400).json({success: false, info: 'Internal error', data: error})
    }
})

app.route('/server/:key')
.get(async (req, res, next) => {
    try {
        const { key } = req.params
        if (!key)
            return res.status(400).json({success: false, info: 'Invalid data structure'})
        let data = await Server.findOne({key})
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        return res.status(400).json({success: false, info: 'Internal server error', data: error})
    }
})

app.listen(3002, () => console.log('Server running on port 3002'))